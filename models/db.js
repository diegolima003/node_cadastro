const Sequelize = require('sequelize')

const sequelize = new Sequelize('cadastro', 'root', 'amocafe', {
    host: 'localhost',
    dialect: 'mysql'
})

module.exports = {
    Sequelize: Sequelize,
    sequelize: sequelize
}