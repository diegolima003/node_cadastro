const db = require('./db')

const User = db.sequelize.define('users', {
    name:{
        type: db.Sequelize.STRING
    },
    email:{
        type: db.Sequelize.STRING
    },
    
    age:{
        type: db.Sequelize.INTEGER
    }    
})

//User.sync({force: true})

module.exports = User 
