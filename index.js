const express = require('express')
const app = express()
const handlebars = require('express-handlebars')
const bodyParser = require('body-parser')

const User = require('./models/User')

//Template Engine
app.engine('handlebars', handlebars({defaultLayout: 'main'}))
app.set('view engine', 'handlebars')

// configuration body-Parser
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())

app.get('/', (req, res)=>{
    res.render('form')
})

//Salva as informações do formulário no banco de dados
app.post('/form', (req, res)=>{
    User.create({
        name: req.body.name,
        email: req.body.email,
        age : req.body.age
    }).then(()=>{
        res.redirect('/users') //vai ser redirecionado para a rota users
    }).catch((erro)=>{
        res.send('Houve um erro'+erro)
    })
})

// Exibindo os usuários cadastrado nessa rota
app.get('/users',(req, res)=> {    
    User.findAll({order: [['id', 'DESC']]}).then((users)=>{
        res.render('users', {users: users})
    })
})
// deletando usuário
app.get('/del/:id', (req, res)=> {
    User.destroy({where:{'id': req.params.id}}).then(()=>{ //deletar pelo id
        res.redirect('/users')             
    }).catch((erro)=>{
        res.send('falha ao deletar'+erro)
    })
})

const port=5500
app.listen(port,()=> console.log('Servido rodando na porta http://localhost:'+port))
